# Optipay 
Web payment solution : 
- aggregate all account in one place (different bank) 
- optimize your payment in using your different account 
- using instant payment 
- virtual account

# Fonctionnalités 
- Authentification sur notre appli client 
- Déclaration de compte 
- Règles d'affection 
- Paiement du partenaire 
- Virement
- Comptes virtuels

# Usages/use case : 
- état de nos comptes 
- visualiser un panier, passer une commande, choisir visa ou optipay, log, paiement, retour sur site marchand
- Regarder l'état des comptes (débit, crédit)

# Répartition des tâches : 
- Yoni : Support, def ressources, regles
- Camille : front, IHM
- Yann : Front, back, DB
- Matthieu : IHM (UX), back, wokflow 
- Alexis : AS YONI
- Maxime : Back, DB, Structure

