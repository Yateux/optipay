package com.optipay;

@Entity
@Table(name = "account")
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "account_id")
    private int accountId;
    private String iban;
    private String bank;

    @Column(name = "account_type")
    private String accountType;

    @Column(name = "bank_account_number")
    private String bankAccountNumber;

    public Account(int accountId, String iban, String bank, String accountType, String bankAccountNumber) {
        this.accountId = accountId;
        this.iban = iban;
        this.bank = bank;
        this.accountType = accountType;
        this.bankAccountNumber = bankAccountNumber;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getBankAccountNumber() {
        return bankAccountNumber;
    }

    public void setBankAccountNumber(String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

}
