package com.optipay;

@Entity
@Table(name = "client")
public class Client {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "client_id")
    public int clientId;
    public String lastname;
    public String firstname;
    public String mail;

    @Column(name = "birth_date")
    @JsonFormat(pattern = "YYYY-MM-dd")
    public Date birthDate;
    public String password;

    public Client(int clientId, String lastname, String firstname, String mail, Date birthDate, String password) {
        this.clientId = clientId;
        this.lastname = lastname;
        this.firstname = firstname;
        this.mail = mail;
        this.birthDate = birthDate;
        this.password = password;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
