package com.optipay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Optipay {
    public static void main(String[] args) {
        SpringApplication.run(Optipay.class, args);
    }
}