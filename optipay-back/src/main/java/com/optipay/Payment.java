package com.optipay;

@Entity
@Table(name = "payment")
public class Payment {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "payment_id")
    private int paymentId;
    private int amount;
    client_id INTEGER NOT NULL,

    @Column(name = "receiver_iban")
    private String receiverIban;


    private int accountId;

    @Column(name = "payment_date")
    private Date paymentDate;

    @Column(name = "seller_company")
    private String sellerCompany;

    @Column(name = "order_number")
    @JsonFormat(pattern = "YYYY-MM-dd")
    private String orderNumber;
}
