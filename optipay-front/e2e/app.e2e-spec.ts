import { OptipayFrontPage } from './app.po';

describe('optipay-front App', () => {
  let page: OptipayFrontPage;

  beforeEach(() => {
    page = new OptipayFrontPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
