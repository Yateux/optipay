// Open Bank Project

// Copyright 2011-2016 TESOBE Ltd.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Open Bank Project (http://www.openbankproject.com)
// Copyright 2011-2016 TESOBE Ltd.

// This product includes software developed at
// TESOBE (http://www.tesobe.com/) contact AT tesobe DOT com
// by:
// Nina Gänsdorfer
// Everett Sochowski
// Stefan Bethge
// Simon Redfern

var express = require('express', template = require('pug'));
var session = require('express-session')
var util = require('util');
var oauth = require('oauth');
var request = require('request');
var path = require("path");
var accounts = require('./account.json');
var types = require('./types.json');
var beneficiaries = require('./beneficiaries.json');
var banks = require('./banks.json');

var app = express();


// To get the values for the following fields, please register your client here:
// https://apisandbox.openbankproject.com/consumer-registration
// Then create a file called config.json in this directory
// and paste your consumer key and secret like this:
//config.json:
//{
//"consumerKey": "YOUR CONSUMER KEY GOES HERE",
//"consumerSecret" : "YOUR CONSUMER SECRET GOES HERE"
//}

// Template engine (previously known as Jade)
var pug = require('pug');

// This loads your consumer key and secret from a file you create.
var config = require('./config.json');

// Used to validate forms
var bodyParser = require('body-parser');


// create application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false });


var _openbankConsumerKey = config.consumerKey;

// The location, on the interweb, of the OBP API server we want to use.
var apiHost = config.apiHost;

console.log ("apiHost is: " + apiHost);

var cookieParser = require('cookie-parser');
app.use(session({
  secret: "very secret",
  resave: false,
  saveUninitialized: true
}));

app.get('/login', function(req, res){
    if(!req.session.token) {
        req.session.getUrlBack = req.header('Referer') || '/';
        var template = "./template/signIn.pug";
        var html = pug.renderFile(template);
        res.status(200).send(html)

    } else {
        res.redirect('/home');
    }
});

app.post('/login', urlencodedParser, function(req, res){
    if(!req.session.token) {
        var template = "./template/signIn.pug";

        if (!req.body) return res.sendStatus(400)

        var username = req.body.username;
        var password = req.body.password;

        var headers = {
            'authorization': 'DirectLogin username=' + username + ', password=' + password + ', consumer_key=' + _openbankConsumerKey,
            'content-type': 'application/json'
        };

        var options = {
            url: apiHost + "/my/logins/direct",
            method: 'post',
            headers: headers
        };

        request(options, function (error, response, data) {
            if (!error && response.statusCode === 200) {
                var json = JSON.parse(data);
                var splited = req.session.getUrlBack.split('/');
                req.session.token = json.token;
                if(splited[3] === "sagazone") {
                    res.redirect('/order-payment');
                } else {
                    res.redirect('/home');
                }
            } else {
                res.redirect('/error');
            }
        });

    } else {
        res.redirect('/home');
    }
});

app.get('/error', function(req, res){
    var template = "./template/error.pug";
    var html = pug.renderFile(template);
    res.status(200).send(html)
});

app.get('/home', function(req, res){
    if(req.session.token) {
        var template = "./template/home.pug";

        var sum = 0;
        var currency = accounts[0].balance.currency;
        for(var i in accounts) {
            sum += parseInt(accounts[i].balance.amount);
            if(parseInt(accounts[i].type) == 200 && accounts[i].debit != undefined) {
                sum -= parseInt(accounts[i].debit);
            }
        }
        var options = { sum: sum , currency: currency, accounts: accounts, types: types, beneficiaries: beneficiaries};
        var html = pug.renderFile(template, options);
        res.status(200).send(html)
    } else {
        res.redirect('/login');
    }
});

app.get('/beneficiaries', function(req, res){
    if(req.session.token) {
        var template = "./template/beneficiaries.pug";
        var options = { beneficiaries: beneficiaries, banks: banks};
        var html = pug.renderFile(template, options);
        res.status(200).send(html)
    } else {
        res.redirect('/login');
    }
});

app.get('/accounts', function(req, res){
    if(req.session.token) {
        var template = "./template/accounts.pug";
        var sum = 0;
        var currency = accounts[0].balance.currency;
        for(var i in accounts) {
            sum += parseInt(accounts[i].balance.amount);
            if(parseInt(accounts[i].type) == 200 && accounts[i].debit != undefined) {
                sum -= parseInt(accounts[i].debit);
            }
        }
        var options = { sum: sum , currency: currency, accounts: accounts, types: types};
        var html = pug.renderFile(template, options);
        res.status(200).send(html)
    } else {
        res.redirect('/login');
    }
});

app.get('/sagazone', function(req, res){
    var template = "./template/sagazone.pug";
    var html = pug.renderFile(template);
    res.status(200).send(html)
});


app.get('/cash-transfer-options', function(req, res){
    var template = "./template/cash-transfer-options.pug";
    var sum = 0;
    var currency = accounts[0].balance.currency;
    for(var i in accounts) {
        sum += parseInt(accounts[i].balance.amount);
        if(parseInt(accounts[i].type) == 200 && accounts[i].debit != undefined) {
            sum -= parseInt(accounts[i].debit);
        }
    }
    var options = { sum: sum , currency: currency, accounts: accounts, types: types};
    var html = pug.renderFile(template, options);
    res.status(200).send(html)
});

app.post('/payment', function(req, res){
    if(req.session.token) {
        var headers = {
            'authorization': 'DirectLogin token=' + req.session.token,
            'content-type': 'application/json'
        };


        var options = {
            url: apiHost + "/obp/v2.1.0/banks/00100/accounts/898786c1-0178-3b2a-8a30-2c983310f08e/accountant/transaction-request-types/SEPA/transaction-requests",
            method: 'get',
            headers: headers
        }

        var content = {  "value":{    "currency":"EUR",    "amount":"10"  },  "to":{    "iban":"123"  },  "description":"This is a SEPA Transaction Request",  "charge_policy":"SHARED"};
        request(options, function (error, response, data) {
            if (!error && response.statusCode == 200) {
                var json = JSON.parse(data);
                req.session.accounts = json;
                for(var i in req.session.accounts) {
                    console.log(req.session.accounts[i]._links.detail.href);
                }
            }
        });

    } else {
        res.redirect('/login');
    }
});

app.get('/sagazone-payment-ok', function(req, res){
    var template = "./template/sagazone-payment-ok.pug";
    var html = pug.renderFile(template);
    res.status(200).send(html)
});

app.get('/cash-transfered', function(req, res){
    var template = "./template/cash-transfered.pug";
    var sum = 0;
    var currency = accounts[0].balance.currency;
    for(var i in accounts) {
        sum += parseInt(accounts[i].balance.amount);
        if(parseInt(accounts[i].type) == 200 && accounts[i].debit != undefined) {
            sum -= parseInt(accounts[i].debit);
        }
    }
    var options = { sum: sum , currency: currency, accounts: accounts, types: types};
    var html = pug.renderFile(template, options);
    res.status(200).send(html)
});

app.get('/order-payment', function(req, res){
    if(req.session.token) {
        var template = "./template/order-payment.pug";
        var sum = 0;
        var currency = accounts[0].balance.currency;
        for(var i in accounts) {
            sum += parseInt(accounts[i].balance.amount);
            if(parseInt(accounts[i].type) == 200 && accounts[i].debit != undefined) {
                sum -= parseInt(accounts[i].debit);
            }
        }
        var options = { sum: sum, currency: currency};
        var html = pug.renderFile(template, options);
        res.status(200).send(html)
    } else {
        res.redirect('/login');
    }
});

app.post('/transfer', function(req, res){
    if(req.session.token) {
        var headers = {
            'authorization': 'DirectLogin token=' + req.session.token,
            'content-type': 'application/json'
        };

        var options = {
            url: apiHost + "/obp/v2.1.0/banks/00100/accounts/898786c1-0178-3b2a-8a30-2c983310f08e/accountant/transaction-request-types/SANDBOX_TAN/transaction-requests",
            method: 'get',
            headers: headers
        };
        var content = {  "to":{    "bank_id":"00100",    "account_id":"99373ff7-d07c-3f87-aa87-7fe2b6e245c2"  },  "value":{    "currency":"XAF",    "amount":"10"  },  "description":"Good"};
        request(options, function (error, response, data) {
            if (!error && response.statusCode == 200) {
                var json = JSON.parse(data);
                req.session.accounts = json;

                for(var i in req.session.accounts) {
                    console.log(req.session.accounts[i]._links.detail.href);
                }
            }
        });

    } else {
        res.redirect('/login');
    }
});

app.get('*', function(req, res){
  res.redirect('/sagazone');
});

app.listen(8085);

